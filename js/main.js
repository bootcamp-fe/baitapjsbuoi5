// Exercise 1
const DIEM_KHU_VUC_A = 2;
const DIEM_KHU_VUC_B = 1;
const DIEM_KHU_VUC_C = 0.5;
const DIEM_DOI_TUONG_1 = 2.5;
const DIEM_DOI_TUONG_2 = 1.5;
const DIEM_DOI_TUONG_3 = 1;



var ex1HanderlerBtn = document.getElementById("ex1-handler-btn");

function diemUuTienKhuVuc(khuVuc){
    switch(khuVuc){
        case "A":
            return DIEM_KHU_VUC_A;
        case "B":
            return DIEM_KHU_VUC_B;
        case "C":
            return DIEM_KHU_VUC_C;
        default:
            return 0;
    }
}

function diemUuTienDoiTuong(doiTuong){
    switch(doiTuong){
        case "1":
            return DIEM_DOI_TUONG_1;
        case "2":
            return DIEM_DOI_TUONG_2;
        case "3":
            return DIEM_DOI_TUONG_3;
        default:
            return 0;
    }
}


function ex1Handler(){
    var ex1Input1 = document.querySelector("#ex1-input #ex-input-1").value*1;
    var ex1Input2 = document.querySelector("#ex1-input #ex-input-2").value;
    var ex1Input3 = document.querySelector("#ex1-input #ex-input-3").value;
    var ex1Input4 = document.querySelector("#ex1-input #ex-input-4").value*1;
    var ex1Input5 = document.querySelector("#ex1-input #ex-input-5").value*1;
    var ex1Input6 = document.querySelector("#ex1-input #ex-input-6").value*1;
    
    var diemChuan = ex1Input1;
    var khuVuc = ex1Input2;
    var diemKhuVuc = diemUuTienKhuVuc(khuVuc);
    var doiTuong = ex1Input3;
    var diemDoiTuong = diemUuTienDoiTuong(doiTuong);
    var diemMon1 = ex1Input4;
    var diemMon2 = ex1Input5;
    var diemMon3 = ex1Input6;

    var diemTong = diemMon1 + diemMon2 + diemMon3 + diemKhuVuc + diemDoiTuong;
    var resultEx1 = "";
    if (diemTong >= diemChuan && diemMon1!=0 && diemMon2!=0 && diemMon3!=0){
        resultEx1 = "Tổng điểm: " + diemTong + ". Bạn đã đậu!" ;
    } else {
            resultEx1 = "Tổng điểm: " + diemTong + ". Bạn đã rớt!";
    }

    document.querySelector('#ex1-result #ex-result-text').innerHTML = resultEx1;
}

// Exercise 2

const GIA_DIEN_50_KW_DAU = 500;
const GIA_DIEN_50_DEN_100_KW = 650;
const GIA_DIEN_100_DEN_200_KW = 850;
const GIA_DIEN_200_DEN_350_KW = 1100;
const GIA_DIEN_TREN_350_KW = 1300;

var ex2HanderlerBtn = document.getElementById("ex2-handler-btn");

function ex2Handler(){
    var ex2Input1 = document.querySelector("#ex2-input #ex-input-1").value;
    var ex2Input2 = document.querySelector("#ex2-input #ex-input-2").value*1;
    var hoTen = ex2Input1;
    var kwTieuThu = ex2Input2;
    var soTienPhaiTra = 0;
    if(kwTieuThu <= 0){
        alert("Mời nhập số kW tiêu thụ hợp lệ!");
        return;
    }
    if(kwTieuThu <= 50 && kwTieuThu>=0){
        soTienPhaiTra = kwTieuThu*GIA_DIEN_50_KW_DAU;
    } else if (kwTieuThu<=100){
        soTienPhaiTra = GIA_DIEN_50_KW_DAU*50 + (kwTieuThu - 50)*GIA_DIEN_50_DEN_100_KW;
    } else if (kwTieuThu<=200){
        soTienPhaiTra = GIA_DIEN_50_KW_DAU*50 + GIA_DIEN_50_DEN_100_KW*50 + (kwTieuThu - 100)*GIA_DIEN_100_DEN_200_KW;
    } else if(kwTieuThu<=350){
        soTienPhaiTra = GIA_DIEN_50_KW_DAU*50 + GIA_DIEN_50_DEN_100_KW*50 + GIA_DIEN_100_DEN_200_KW*100 +(kwTieuThu - 200)*GIA_DIEN_200_DEN_350_KW;
    } else {
        soTienPhaiTra = GIA_DIEN_50_KW_DAU*50 + GIA_DIEN_50_DEN_100_KW*50 + GIA_DIEN_100_DEN_200_KW*100 + GIA_DIEN_200_DEN_350_KW*150 +(kwTieuThu -350)*GIA_DIEN_TREN_350_KW;
    }

    var resultEx2 = "Khách hàng: " + hoTen +". Số tiền phải trả: " + new Intl.NumberFormat('vi-VN', { style: 'currency', currency: 'VND' }).format(soTienPhaiTra);

    document.querySelector('#ex2-result #ex-result-text').innerHTML = resultEx2;

}

// Exercise 3

var ex3HanderlerBtn = document.getElementById("ex3-handler-btn");

function ex3Handler(){
    var ex3Input1 = document.querySelector("#ex3-input #ex-input-1").value;
    var ex3Input2 = document.querySelector("#ex3-input #ex-input-2").value;
    var ex3Input3 = document.querySelector("#ex3-input #ex-input-3").value;
    var oddNums = 0;
    var evenNums =0;
    var resultEx3;
    if(ex3Input1!="" && ex3Input2!="" &&ex3Input3!=""){
        if(ex3Input1%2!=0){
            oddNums++;
        } 
        if(ex3Input2%2!=0){
            oddNums++;
        }
        if(ex3Input3%2!=0){
            oddNums++;
        }
        evenNums = 3 - oddNums;
        resultEx3 = "Có " + evenNums + " số chẵn, " + oddNums + " số lẻ";
    } else {
        resultEx3 = "Không được để trống ô input!"
    }
    
    document.querySelector('#ex3-result #ex-result-text').innerHTML = resultEx3;
}

// Exercise 4

var ex4HanderlerBtn = document.getElementById("ex4-handler-btn");

function ex4Handler(){
    var ex4Input1 = document.querySelector("#ex4-input #ex-input-1").value*1;
    var ex4Input2 = document.querySelector("#ex4-input #ex-input-2").value*1;
    var ex4Input3 = document.querySelector("#ex4-input #ex-input-3").value*1;
    var canh1 = ex4Input1;
    var canh2 = ex4Input2;
    var canh3 = ex4Input3;
    var resultEx4;
    if(Math.abs(canh2 - canh3)<canh1 && canh1 < (canh2 + canh3) && canh1*canh2*canh3 > 0){
        if(canh1 == canh2 && canh1 == canh3 && canh2 == canh3 ){
            resultEx4 = "Đây là một tam giác đều !";
        } else if(canh1 == canh2 || canh1 == canh3 || canh2 == canh3){
            if((canh1*canh1 + canh2*canh2) == canh3*canh3 || (canh1*canh1 + canh3*canh3) == canh2*canh2 || (canh2*canh2 + canh3*canh3) == canh1*canh1){
                resultEx4 = "Đây là tam giác vuông cân !";
            } else {
                resultEx4 = "Đây là tam giác cân !";
            }
        } else if((canh1*canh1 + canh2*canh2) == canh3*canh3 || (canh1*canh1 + canh3*canh3)==canh2*canh2 || (canh2*canh2 + canh3*canh3) == canh1*canh1){
            resultEx4 = "Đây là tam giác vuông !"
        } else {
            resultEx4 = "Không phải là một tam giác đặc biệt !";
        }
    } else {
        resultEx4 = "Không tồn tại tam giác trên !";
    }

    document.querySelector('#ex4-result #ex-result-text').innerHTML = resultEx4;
}